package src.shop.service;

import src.shop.interfaces.IDepartment;
import src.shop.interfaces.IEmployee;

public class BaseEmployees implements IEmployee {

    private IDepartment department;
    private String name;
    private boolean free;

    public BaseEmployees(IDepartment department, String name, boolean free) {
        this.department = department;
        this.name = name;
        this.free = free;
    }

    @Override
    public IDepartment getDepartment() {
        return department;
    }

    public BaseEmployees(boolean free) {
        this.free = free;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}

package src.shop.service;

import src.shop.departments.BaseDepartment;

public class Cashier extends BaseEmployees {

    public Cashier(BaseDepartment department, String name, boolean free) {
        super(department, name, free);
    }

    public void getMoney() {

    }

    public void giveBonusCard() {

    }

    public Cashier(boolean free) {
        super(free);
    }

}

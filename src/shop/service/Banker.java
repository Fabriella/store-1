package src.shop.service;

import src.shop.bank.BaseBank;
import src.shop.departments.BaseDepartment;

public class Banker extends src.shop.service.BaseEmployees {

    private BaseBank bank;
    
    public Banker(BaseDepartment department, String name, boolean free, BaseBank bank) {
        super(department, name, free);
        this.bank = bank;
    }

    public void sendRequest() {

    }

    public Banker(boolean free) {
        super(free);
    }

}

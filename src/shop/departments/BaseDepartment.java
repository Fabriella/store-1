package src.shop.departments;

import src.shop.goods.BaseGoods;
import src.shop.interfaces.IDepartment;
import src.shop.interfaces.IEmployee;
import src.shop.interfaces.IGood;
import src.shop.service.BaseEmployees;

import java.util.ArrayList;

public abstract class BaseDepartment implements IDepartment {

    private String name;
    ArrayList<IGood> goodList=new ArrayList<>();
    ArrayList<IEmployee> employeeList=new ArrayList<>();
    
    public BaseDepartment(String name, ArrayList<IGood> goodList, ArrayList<IEmployee> employeeList) {
        this.name = name;
        this.goodList = goodList;
        this.employeeList = employeeList;
    }

    public BaseDepartment() {

    }

    public void addEmployee(BaseEmployees baseEmployees) {
        employeeList.add(baseEmployees);
    }

    public void addGood(BaseGoods baseGoods) {
        goodList.add(baseGoods);
    }

    public String getName() {
        return name;
    }

    public ArrayList<IGood> getGoodList() {
        return goodList;
    }

    public ArrayList<IEmployee> getEmployeeList() {
        return employeeList;
    }

}

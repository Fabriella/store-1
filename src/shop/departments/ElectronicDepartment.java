package src.shop.departments;

import src.shop.interfaces.IEmployee;
import src.shop.interfaces.IGood;

import java.util.ArrayList;

public class ElectronicDepartment extends BaseDepartment {

    public ElectronicDepartment(String name, ArrayList<IGood> goodList, ArrayList<IEmployee> employeeList) {
        super(name, goodList, employeeList);
    }

    public ElectronicDepartment() {
        super();
    }
}

package src.shop.start;

import src.shop.SalesRoom;
import src.shop.clients.Visitor;
import src.shop.departments.ElectronicDepartment;
import src.shop.departments.GameDepartment;
import src.shop.goods.Computer;
import src.shop.goods.GameConsole;
import src.shop.goods.HardDrive;
import src.shop.goods.Televisor;
import src.shop.service.*;
import src.shop.enums.ConsultResult;

public class Main {

    public static String SHOP_NAME = "ДНС";

    public static void main(String[] args) {

        imitateShopWorking();

    }

    public static void imitateShopWorking(){

        SalesRoom salesRoom=new SalesRoom(SHOP_NAME);
        Administrator admin=new Administrator("AdminName");
        ElectronicDepartment electronicDepartment=new ElectronicDepartment();
        GameDepartment gameDepartment=new GameDepartment("GameDepartmentName");

        salesRoom.addDepartment(electronicDepartment);
        salesRoom.addDepartment(gameDepartment);

        Banker bankerElecDep=new Banker(true);
        Cashier cashierElecDep=new Cashier(true);
        Consultant consultantElecDep=new Consultant(true);
        Security securityElecDep=new Security(true);

        Banker bankerGame=new Banker(true);
        Cashier cashierGame=new Cashier(true);
        Consultant consultantGame=new Consultant(true);
        Security securityGame=new Security(true);

        electronicDepartment.addEmployee(bankerElecDep);
        electronicDepartment.addEmployee(cashierElecDep);
        electronicDepartment.addEmployee(consultantElecDep);
        electronicDepartment.addEmployee(securityElecDep);

        gameDepartment.addEmployee(bankerGame);
        gameDepartment.addEmployee(cashierGame);
        gameDepartment.addEmployee(consultantGame);
        gameDepartment.addEmployee(securityGame);

        Computer computer=new Computer("computer");
        GameConsole gameConsole=new GameConsole("console");
        HardDrive hardDrive=new HardDrive("HD");
        Televisor televisor=new Televisor("TV");

        electronicDepartment.addGood(computer);
        gameDepartment.addGood(gameConsole);
        electronicDepartment.addGood(hardDrive);
        electronicDepartment.addGood(televisor);

        Visitor firstVisitor=new Visitor("FirstVisitor");
        Visitor secondVisitor=new Visitor("SecondVisitor");

        Consultant forFirstVisitor=admin.getFreeConsultant(electronicDepartment);
        Consultant forSecondVisitor=admin.getFreeConsultant(gameDepartment);

        ConsultResult consultResultForFirst=forFirstVisitor.consult(firstVisitor);
        ConsultResult consultResultForSecond=forSecondVisitor.consult(secondVisitor);

        switch (consultResultForFirst){
            case BUY:
                firstVisitor.buy(televisor);
                break;
            case EXIT:
                break;
        }

        switch (consultResultForSecond){
            case BUY:
                secondVisitor.buy(gameConsole);
                break;
            case EXIT:
                break;
        }

    }

}

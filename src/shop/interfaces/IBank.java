package src.shop.interfaces;

public interface IBank {

    void checkInfo();
    void giveCredit();
    String getName();
    String getCreditDescription();


}

package src.shop.interfaces;

public interface IGood {

    IDepartment getDepartment();
    String getCompany();
    String getName();
    String getPrice();
    boolean isHasGuarantee();

}

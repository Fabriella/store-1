package src.shop.interfaces;

import src.shop.goods.BaseGoods;
import src.shop.service.BaseEmployees;

import java.util.ArrayList;

public interface IDepartment {

    String getName();
    ArrayList<IGood> getGoodList();
    ArrayList<IEmployee> getEmployeeList();
    void addGood(BaseGoods baseGoods);
    void addEmployee(BaseEmployees baseEmployees);

}

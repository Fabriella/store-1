package src.shop.goods;

import src.shop.interfaces.IDepartment;
import src.shop.interfaces.IElectronicDevice;

public class ElectronicDevice extends BaseGoods implements IElectronicDevice {

    public ElectronicDevice(IDepartment department, String name, boolean hasGuarantee, String price, String company) {
        super(department, name, hasGuarantee, price, company);
    }

    public ElectronicDevice(String name){
        super(name);
    }

}
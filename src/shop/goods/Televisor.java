package src.shop.goods;

import src.shop.departments.BaseDepartment;

public class Televisor extends ElectronicDevice {

    private String model;

    public Televisor(BaseDepartment department, String name, boolean hasGuarantee, String price, String company, String model) {
        super(department, name, hasGuarantee, price, company);
        this.model = model;
    }

    public Televisor(String name){
        super(name);
    }

    public void on() {

    }

    public void off() {

    }

    public void selectChannel() {

    }

}

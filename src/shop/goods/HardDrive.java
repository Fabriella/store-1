package src.shop.goods;

import src.shop.departments.BaseDepartment;

public class HardDrive extends ElectronicDevice {

    private String volume;

    public HardDrive(BaseDepartment department, String name, boolean hasGuarantee, String price, String company, String volume) {
        super(department, name, hasGuarantee, price, company);
        this.volume = volume;
    }

    public void format() {

    }

    public void copy() {

    }

    public void delete() {

    }

    public HardDrive(String name) {
        super(name);
    }
}
